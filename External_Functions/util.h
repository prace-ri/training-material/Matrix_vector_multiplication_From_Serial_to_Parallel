/*
 * util.h -- Some usefull functions for error checking
 * 
 * Author: Petros Anastasiadis(panastas@cslab.ece.ntua.gr) 
 */

#include "timer.h"

#define NR_ITER 100

void error(const char * msg); /* A function for error printing and exiting */
void report_results(double time); /* Print timer results */
void report_mpi_results(double comm_timer, double comp_timer);

void check_result(double *test, double *orig, size_t n); /* Check vector result  */
int vec_equals(const double *v1, const double *v2, size_t n, double eps); /* Check vector v1[n], v2[n] equality with 'eps' precision  */
