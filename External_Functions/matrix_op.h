/*
 *  
 *  matrix_op.h -- Basic Matrix transform/split operations
 *  
 *	Author: Petros Anastasiadis(panastas@cslab.ece.ntua.gr) 
 *
 */ 

void vec_init(double *v, size_t n, double val); 						/* Initialize n vector to 'val' */
void vec_init_rand(double *v, size_t n, double max);					/* Initialize n vector to random values between 0 and 'max' (Constant seed for error checking) */
void vec_init_rand_p(double *v, size_t n, size_t np, double max);		/* Initialize n+np vector to random values between 0 and 'max' for n elems and 0.0 for padding */
void matrix_init_rand(double **v, size_t n, size_t m, double max);		/* Initialize v[n][n] matrix to random values between 0 and 'max' */
void ser_matrix_init_rand(double *v, size_t n, size_t m, double max);	/* Initialize v[n*m] matrix to random values between 0 and 'max' */
void ser_matrix_init_rand_p(double *v, size_t n, size_t m, size_t np, double max); /* Initialize v[n*m+np] matrix to random values between 0 and 'max' for n*m elems and 0.0 for padding */

void matrix_col_major(double *M, double *A, size_t n, size_t m);		/* Transform row major M[n*m] to column major A[n*m]  */
void matrix_row_major(double **M, double *A, size_t n, size_t m);		/* Transform column major M[n][m] to column major A[n*m]  */

void regenerate_matrix_coo(double **M, int * I, int * cooCol, double * cooVal, int n, int m, int n_z);	/* Generate (sparse?) M[n][m] matrix from given COO format  */
