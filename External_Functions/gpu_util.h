/*
 *  some GPU utility functions
 *  Author: Petros Anastasiadis(panastas@cslab.ece.ntua.gr) 
 *	Use of Unified memory is advised, but not always optimal
 */  

void gpu_free(void *gpuptr);									/* Free GPU memory*/
void *gpu_alloc(size_t count);									/* Allocate 'count' bytes in GPU memory (error safe) */
int copy_to_gpu(const void *host, void *gpu, size_t count);		/* Copy 'count' bytes from host to gpu memory (error safe)  */
int copy_from_gpu(void *host, const void *gpu, size_t count);	/* Copy 'count' bytes from gpu to host memory (error safe) */

void cudaCheckErrors(const char * msg);							/* Check GPU for errors */
const char *gpu_get_errmsg(cudaError_t err);
const char *gpu_get_last_errmsg();
