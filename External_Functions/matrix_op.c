/*
 *  
 *  matrix_op.c -- Basic Matrix transform/split operations
 *  
 *	Author: Petros Anastasiadis(panastas@cslab.ece.ntua.gr) 
 *
 */ 

#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "matrix_op.h"

void vec_init(double *v, size_t n, double val)
{
    size_t  i;
    for (i = 0; i < n; ++i) {
        v[i] = val;
    }
}

void vec_init_rand(double *v, size_t n, double max)
{
    srand48(42);   // should only be called once
    size_t  i;
    for (i = 0; i < n; ++i) {
        v[i] = max*(double) drand48();
    }
}

void vec_init_rand_p(double *v, size_t n, size_t np, double max)
{
    srand48(42);   // should only be called once
    size_t  i;
    for (i = 0; i < n; ++i) {
        v[i] = (double) drand48();
    }
	for (i = n; i < np; ++i) {
        v[i] = 0.0;
    }
}

void matrix_init_rand(double **v, size_t n, size_t m, double max)
{
    srand48(42);   // should only be called once
    size_t  i,j;
    for (i = 0; i < n; ++i) 
		for (j = 0; j < m; ++j) 
        	v[i][j] = max*(double) drand48();
    
}

void ser_matrix_init_rand(double *v, size_t n, size_t m, double max)
{
    srand48(42);   // should only be called once
    size_t  i,j;
    for (i = 0; i < n; ++i) 
		for (j = 0; j < m; ++j) 
        	v[i*m+j] = max*(double) drand48();
    
}

void ser_matrix_init_rand_p(double *v, size_t n, size_t m, size_t np, double max)
{
    srand48(42);   // should only be called once
    size_t  i,j;
    for (i = 0; i < n; ++i) 
		for (j = 0; j < m; ++j) 
        	v[i*m+j] = max*(double) drand48();
    for (i = n*m; i < n*m+np; ++i) v[i] = 0.0;
}

void matrix_col_major(double *M, double *A, size_t n, size_t m)
{
	size_t  i,j;
    for (i = 0; i < n; ++i) 
		for (j = 0; j < m; ++j)
			A[j*n+i] = M[i*m+j];
}
	
void matrix_row_major(double **M, double *A, size_t n, size_t m)
{
	size_t  i,j;
    for (i = 0; i < n; ++i) 
		for (j = 0; j < m; ++j)
			A[i*n+j] = M[i][j];
}
	

void regenerate_matrix_coo(double **M, int *I, int *cooCol, double *cooVal, int n, int m, int n_z)
{
	int  i;
    for (i = 0; i < n_z; ++i) M[I[i]][cooCol[i]] = cooVal[i];
}


