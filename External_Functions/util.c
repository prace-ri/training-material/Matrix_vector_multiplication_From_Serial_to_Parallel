/*
 * util.c -- Some usefull functions for error checking
 * 
 * Author: Petros Anastasiadis(panastas@cslab.ece.ntua.gr) 
 */

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "util.h"

void error(const char * msg)
{
	perror(msg);
	exit(1);
}

void check_result(double *test, double *orig, size_t n)
{
	size_t  i_fail = vec_equals(test, orig, n, 0.00001);
	if (!i_fail) ; //printf("Checked, ");
	else printf("FAILED %ld times", i_fail );
}

void report_results(double timer)
{
	printf("t= %lf ms\n",1000.0/NR_ITER*timer);
}

void report_mpi_results(double comm_timer, double comp_timer)
{
	printf("comp_t= %lf ms, comm_t= %lf ms\n",1000.0/NR_ITER*comp_timer, 1000.0*comm_timer);
}

int vec_equals(const double *v1, const double *v2, size_t n, double eps)
{
	size_t  i,k=0;
    	for (i = 0; i < n; ++i) {
		if (fabs(v1[i] - v2[i]) > eps) k++;	
    	}
	return k;
}
