/*
 * Some basic functions for mtx reading and formating
 * 
 * Author: Petros Anastasiadis(panastas@cslab.ece.ntua.gr) 
 */

/* a quicksort implementation (required for sorted coo impementation) */
void quickSort( int *a, int * b, double * c, int l, int r);
int partition( int *a, int * b, double * c, int l, int r);

/* Read functions for sparce matrices (mtx format) */
void get_nz_symmetric( int * n_z, char* name);
int mtx_read(int ** I, int ** cooCol, double ** cooVal, int * n, int * m, int * n_z, char * name);
void csr_transform(double **, int, int, int, double *, int *, int *);
int read_mtx_coo(char *, int *, int * , double *, int *, int * , int * );




