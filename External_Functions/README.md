
# Basic helpfull functions used by some of the programs. 

### Function explanation and usage included in corresponding header(.h) files.

Modify at your own risk!

```
->input.c
20/05/2017: Completed

->util.c
06/09/2017: Completed

->matrix_op.c
13/09/2017: Completed
14/09/2017: Modified for array transpose

->gpu_util.c
14/09/2017: Completed

->timer.c
06/09/2017: Completed
19/09/2017: Modified for better precision
```
