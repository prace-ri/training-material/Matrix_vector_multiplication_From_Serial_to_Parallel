DEBUG ?= 0  # Set to 1 for debug

# Need to -I this for user-defined functions to work
EXT_DIR = ../External_Functions/

MPI_PREFIX = $(I_MPI_ROOT)
CUDA_PREFIX = $(CUDAROOT)

#compile with gcc
CC=gcc
CPP=g++
MPICC=mpicc
NVCC=nvcc
CFLAGS=-O3 -lm -Wall -mavx -march=ivybridge -mtune=ivybridge -lrt
CPU_COMPILE= $(CC) $(CFLAGS) -I$(EXT_DIR)
CPU_COMPILE_OMP = $(CPU_COMPILE) -fopenmp
MPI_COMPILE= $(MPICC) -I$(EXT_DIR)
MPI_OMP_COMPILE= $(MPI_COMPILE) -fopenmp
GPU_MPI_CXX = $(NVCC) -L $(I_MPI_ROOT)/lib64 -lmpi -ccbin mpiicc
LDFLAGS ?=-L $(CUDA_PREFIX)/lib64 -lcudart -lcublas -lcusparse -lm -lrt 
GPU_COMPILE = $(NVCC) -I $(CUDA_PREFIX)/include  -arch sm_35 -I$(EXT_DIR) $(LDFLAGS)
GPU_MPI_COMPILE = $(GPU_MPI_CXX) -I $(CUDA_PREFIX)/include -I $(I_MPI_ROOT)/include -arch sm_35 -I$(EXT_DIR) $(LDFLAGS)
CPU_COMPILE_CUDA = $(CPP) $(CFLAGS) -I$(EXT_DIR) $(LDFLAGS)

#compile with icc
ICC =icc
MPICC=mpiicc
ICFLAGS=-O3 -Wall -axCORE-AVX2,CORE-AVX-I 
#CPU_COMPILE= $(ICC) $(ICFLAGS) -I$(EXT_DIR)
#CPU_COMPILE_OMP = $(CPU_COMPILE) -qopenmp
#MPI_COMPILE= $(MPICC) $(ICFLAGS) -I$(EXT_DIR)
#MPI_OMP_COMPILE= $(MPI_COMPILE) -mt_mpi -qopenmp

ifeq ($(DEBUG), 1)
	CPU_COMPILE += -D_DEBUG_
endif

CPU_COMPILE_OBJ= $(CPU_COMPILE) -c
CPU_COMPILE_OMP_OBJ= $(CPU_COMPILE_OMP) -c
MPI_COMPILE_OBJ= $(MPI_COMPILE) -c
CPU_COMPILE_CUDA_OBJ= $(CPU_COMPILE_CUDA) -c
GPU_COMPILE_OBJ= $(GPU_COMPILE) -c




