# A parallel MPI implementation of the matrix-vector multiplication algorithm

```
->MPI(Basic implementation using intel mpi for compilation)
19/09/2017: Completed

->MPI-OpenMP(Hybrid implementation with MPI for data management between nodes and OpenMP for computations)
20/09/2017: Completed

Tested environments:
- Ivy Bridge Intel Xeon E5-2680v2 CPU with Linux x86_64 and intelmpi/5.0.3, intel/15.0.3

```
