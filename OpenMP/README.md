# Two OpenMP impementations of the Matrix-Vector algorithm:

```
->OpenMP (a simple naive parallel for implementation)
07/09/2017: Completed

->OpenMP_aff(matrix initialization with first touch policy to minimize socket memory transactions. Threads are bind to certain cores)
13/09/2017: Completed
18/09/2017: Added thread binding to match memory alocation pattern

Tested environments:
- Ivy Bridge Intel Xeon E5-2680v2 CPU with Linux x86_64 and intelmpi/5.0.3, intel/15.0.3
- SandyBridge Intel Xeon E5-4650v2 CPU with Linux x86_64 and intelmpi/5.0.3, intel/15.0.3

```


