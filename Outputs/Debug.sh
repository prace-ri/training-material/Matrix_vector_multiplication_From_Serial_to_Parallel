# Put all debug files(.debug) for common run values to a single 'Debug' directory in order to compare them
cd Debug

for filename in *
do
	for filename1 in *
	do
		diff $filename $filename1
	done
done
