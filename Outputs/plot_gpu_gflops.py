import matplotlib.pyplot as plt
import numpy as np

def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = float(rect.get_height())
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%.4f' % float(height),
                ha='center', va='bottom')

with open('GPU.out') as file:
	list0 = file.readlines()
with open('Serial.out') as file:
	list1 = file.readlines()
nm=["naive_cuda", "coalesced_cuda", "shmem_cuda", "cuBLAS"]
res0=[]
res=[]
res1=[]
res2=[]
res3=[]
nm=[]
for i in range(0,3):
	n = int(((list0[4*i].split()[3]).split('=')[1])[:-1])
	m = int(((list0[4*i].split()[4]).split('=')[1])[:-2])
	nm.append(str(n) + "x" + str(m))
	#print " n=" + str(n) + ", m=" + str(m)  + ")"
	GFlops_ms = 2 * n * m *1000/1.e9
	res0.append( GFlops_ms/float(list0[4*i].split()[6]))
	res.append(  GFlops_ms/float(list0[4*i+1].split()[7]))
	res1.append( GFlops_ms/float(list0[4*i+2].split()[7]))
	res2.append( GFlops_ms/float(list0[4*i+3].split()[6]))
	res3.append( GFlops_ms/float(list1[i].split()[4]))


fig, ax = plt.subplots(figsize=(16, 9),dpi=300)
N = 3
ind = np.arange(N)  # the x locations for the groups
width = 0.10 # the width of the bars	

rect0 = ax.bar(ind + 0.15, res0, width, color='r',label='naive_cuda')
rect  = ax.bar(ind + 0.30, res, width, color='b',label='coalesced_cuda')
rect1 = ax.bar(ind + 0.45, res1, width, color='g',label='shmem_cuda')
rect2 = ax.bar(ind + 0.60, res2, width, color='k',label='cuBLAS')
rect3 = ax.bar(ind, res3, width, color='c',label='Serial')
ax.set_ylabel('GFLOPS/s')
ax.set_xlabel('Array_size')
ax.set_xticks(ind + width / 2)
ax.set_xticklabels(nm)#rotation = (60))
#autolabel(rect)
ax.set_title( "GPU Benchmark" )
ax.legend(prop={'size':10},loc=2)
plt.savefig('GPU_version_gflops.png', bbox_inches='tight')
plt.close()


	
