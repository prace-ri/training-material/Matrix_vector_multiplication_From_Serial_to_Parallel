import matplotlib.pyplot as plt
import numpy as np

def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = float(rect.get_height())
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%.4f' % float(height),
                ha='center', va='bottom')

with open('GPU.out') as file:
	list0 = file.readlines()
with open('Serial.out') as file:
	list1 = file.readlines()
with open('mpi.out') as file:
	list2 = file.readlines()
nodes = [1,2,4,8,16]
nm=[]
fig, ax = plt.subplots(figsize=(16, 9),dpi=300)
width = 0.1 # the width of the bars
col=['g','b','k','m','c','y']
N=len(nodes)
ind = np.arange(N)  # the x locations for the groups
fig, ax = plt.subplots(figsize=(16, 9),dpi=300)
for i in range(0,3):
	n = int(((list0[4*i].split()[3]).split('=')[1])[:-1])
	m = int(((list0[4*i].split()[4]).split('=')[1])[:-2])
	nm.append(str(n) + "x" + str(m))
	#print " n=" + str(n) + ", m=" + str(m)  + ")"
	GFlops_ms = 2 * n * m *1000/1.e9
	res0=[]
	res=[]
	for j in range(0,N):
		res0.append( GFlops_ms/float(list0[12+3*j+i].split()[-5]))
		res.append(  GFlops_ms/float(list2[6*i+18*j+5].split()[-5]))
	rect0 = ax.bar(ind + 0.3*i, res0, width, color=col[2*i],label='MPI-cuBLAS(2 GPUs/node)'+ nm[i])
	rect  = ax.bar(ind + 0.3*i+0.15, res, width, color=col[2*i+1],label='MPI-OpenMP(20 threads/node)'+ nm[i])
ax.set_ylabel('GFLOPS/s')
ax.set_xlabel('Nodes')
ax.set_xticks(ind)
ax.set_xticklabels(nodes)#rotation = (60))
#autolabel(rect)
ax.set_title( "Multinode Gflops" )
ax.legend(prop={'size':10},loc=2)
plt.savefig("Node_flops.png", bbox_inches='tight')
plt.close()


	
