import matplotlib.pyplot as plt
import numpy as np

def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = float(rect.get_height())
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%.4f' % float(height),
                ha='center', va='bottom')

with open('GPU.out') as file:
	list0 = file.readlines()
with open('Serial.out') as file:
	list1 = file.readlines()
with open('mpi.out') as file:
	list2 = file.readlines()
nodes = [1,2,4,8,16]
nm=[]
width = 0.1 # the width of the bars
col=['g','b','k','m','c','y']
N=len(nodes)
ind = np.arange(N)  # the x locations for the groups
fig, ax = plt.subplots(figsize=(16, 9),dpi=300)
for i in range(0,3):
	n = int(((list0[4*i].split()[3]).split('=')[1])[:-1])
	m = int(((list0[4*i].split()[4]).split('=')[1])[:-2])
	nm.append(str(n) + "x" + str(m))
	res0=[]
	res=[]
	serial = float(list1[i].split()[4])
	for j in range(0,N):
		gpu = float(list0[12+3*j+i].split()[-5])
		omp = float(list2[6*i+18*j+5].split()[-5])
		print "(" + str(i) + "," + str(nodes[j]) + ")gpu= "+ str(gpu) + ",omp= " + str(omp)
		res0.append( serial/ gpu)
		res.append(  serial/ omp)
	rect0 = ax.bar(ind + 0.3*i, res0, width, color=col[2*i],label='MPI-cuBLAS(2 GPUs/node)'+ nm[i])
	rect  = ax.bar(ind + 0.3*i + 0.15, res, width, color=col[2*i+1],label='MPI-OpenMP(20 threads/node)'+ nm[i])
ax.set_ylabel('Speedup')
ax.set_xlabel('Nodes')
ax.set_xticks(ind)
ax.set_xticklabels(nodes)#rotation = (60))
#autolabel(rect)
ax.set_title( "Multinode Speedup" )
ax.legend(prop={'size':10},loc=2)
plt.savefig("Node_Speedup.png", bbox_inches='tight')
plt.close()


	
