# Run all files. Must update if changes are made in directory tree
submit=sbatch
cd ../Serial
make
$submit Serial.slurm 25000
cd ../OpenMP
make
$submit OpenMP.slurm 25000
cd ../GPUs
make
$submit GPU.slurm 25000
cd ../MPI
make
$submit MPI.slurm 25000

