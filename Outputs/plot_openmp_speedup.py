import matplotlib.pyplot as plt
import numpy as np

def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = float(rect.get_height())
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%.4f' % float(height),
                ha='center', va='bottom')

with open('OpenMP.out') as file:
	list0 = file.readlines()
with open('Serial.out') as file:
	list1 = file.readlines()
nm=["naive_cuda", "coalesced_cuda", "shmem_cuda", "cuBLAS"]
cores=[1,2,5,10,20,40]
col=['g','b','k','m','c','y']
res1=[]
res2=[]
res3=[]
nm=[]
fig, pl = plt.subplots(figsize=(16, 9),dpi=300)
for i in range(0,3):
	n = int(((list1[i].split()[1]).split('=')[1])[:-1])
	m = int(((list1[i].split()[2]).split('=')[1])[:-2])
	nm.append(str(n) + "x" + str(m))
	serial = float(list1[i].split()[4])
	res0=[]
	res=[]
	print " n=" + str(n) + ", m=" + str(m)  + ")"
	for j in range(0,len(cores)):		
	#GFlops_ms = 2 * n * m *1000/1.e9
		res0.append( serial/float(list0[12*i+2*j].split()[5]))
		res.append(  serial/float(list0[12*i+2*j+1].split()[5]))
	y = plt.plot(cores,res0, color = col[2*i], label="OpenMP_Naive("+nm[i]+")")
	y = plt.plot(cores,res, color = col[2*i+1], label="OpenMP_Aff("+nm[i]+")" )


y = plt.plot(cores,cores, color='r',label='Linear Speedup')
pl.set_ylabel('Speedup')
pl.set_xlabel('Cores')
pl.set_xlim([1,40])
pl.set_ylim([1,40])
#ax.set_xticks(ind + width / 2)
#pl.set_xticklabels(cores)#rotation = (60))
#autolabel(rect)

pl.set_title( "OpenMP Benchmark" )
pl.legend(prop={'size':10},loc=2)
plt.savefig('OpenMP_version_speedup.png', bbox_inches='tight')
plt.close()


	
