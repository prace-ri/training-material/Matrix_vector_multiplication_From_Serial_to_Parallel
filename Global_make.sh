#!/bin/bash

## LOAD MODULES ##
module purge		# clean up loaded modules 

# load necessary modules
module load gnu
module load intel
module load intelmpi
module load binutils
module load cuda

for n;
do
	if cd "$n" ; 
	then
		make
		cd ../
	else
		if [ "$n" == "-all" ];
		then
			cd GPUs  
			make  
			cd ../MPI  
			make  
			cd ../OpenMP  
			make
		else
			echo "Use: ./Global_make.sh Prog_dir_name or ./Global_make.sh -all"
		fi		
	fi
done
