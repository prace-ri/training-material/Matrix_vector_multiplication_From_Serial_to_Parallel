
#include <stdio.h>

/*
 *  Utility function to get the thread ID within the
 *  global working space.
 */ 
__device__ int get_global_tid();

/*
 *  Utility function to get the thread ID within the
 *  local/block working space.
 */ 
__device__ int get_local_tid();

/*
 *  Naive kernel
 */ 
__global__ void dmv_gpu_naive(const double *a, const double *x, double *y, size_t n);

/*
 *  Coalesced memory acceses
 */
__global__ void dmv_gpu_coalesced(const double *a, const double *x, double *y, size_t n);


/*
 *  Use of shared memory
 */
__global__ void dmv_gpu_shmem(const double *a, const double *x, double *y, size_t n);



